package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import model.Atleta;
import model.Endereco;
import controller.ControladorAtleta;

public class JanelaAtleta extends JFrame {

	ControladorAtleta umControlador = new ControladorAtleta();
	Atleta umAtleta = new Atleta();
	Endereco umEndereco = new Endereco();
	
	private JPanel contentPane;
	private JLabel lblNome;
	private JLabel lblIdade;
	private JLabel lblPeso;
	private JTextField nomeField;
	private JTextField pesoField;
	private JTextField idadeField;
	private JLabel lblEstadouf;
	private JTextField estadoField;
	private JLabel lblCidade;
	private JTextField cidadeField;
	private JLabel lblLogradouro;
	private JTextField logradouroField;
	private JLabel lblNumero;
	private JButton btnCadastrar;
	private JTextField numeroField;
	private JButton btnRemover;
	private JButton btnBuscar;
	private final Action action = new SwingAction();
	private final Action action_1 = new SwingAction_1();
	private final Action action_2 = new SwingAction_2();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JanelaAtleta frame = new JanelaAtleta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JanelaAtleta() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 61, 61, 27);
		contentPane.add(lblNome);
		
		lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(10, 46, 73, 81);
		contentPane.add(lblIdade);
		
		lblPeso = new JLabel("Peso:");
		lblPeso.setBounds(10, 100, 61, 27);
		contentPane.add(lblPeso);
		
		nomeField = new JTextField();
		nomeField.setBounds(63, 50, 200, 19);
		contentPane.add(nomeField);
		nomeField.setColumns(10);
		
		pesoField = new JTextField();
		pesoField.setBounds(63, 108, 200, 19);
		contentPane.add(pesoField);
		pesoField.setColumns(10);
		
		idadeField = new JTextField();
		idadeField.setBounds(63, 77, 200, 19);
		contentPane.add(idadeField);
		idadeField.setColumns(10);
		
		lblEstadouf = new JLabel("Estado (UF):");
		lblEstadouf.setBounds(10, 146, 96, 32);
		contentPane.add(lblEstadouf);
		
		estadoField = new JTextField();
		estadoField.setBounds(112, 153, 151, 19);
		contentPane.add(estadoField);
		estadoField.setColumns(10);
		
		lblCidade = new JLabel("Cidade:");
		lblCidade.setBounds(10, 183, 73, 27);
		contentPane.add(lblCidade);
		
		cidadeField = new JTextField();
		cidadeField.setBounds(79, 191, 184, 19);
		contentPane.add(cidadeField);
		cidadeField.setColumns(10);
		
		lblLogradouro = new JLabel("Logradouro:");
		lblLogradouro.setBounds(10, 222, 96, 19);
		contentPane.add(lblLogradouro);
		
		logradouroField = new JTextField();
		logradouroField.setBounds(116, 220, 147, 21);
		contentPane.add(logradouroField);
		logradouroField.setColumns(10);
		
		lblNumero = new JLabel("Numero:");
		lblNumero.setBounds(10, 247, 96, 19);
		contentPane.add(lblNumero);
		
		btnCadastrar = new JButton("CADASTRAR");
		btnCadastrar.setAction(action);
		btnCadastrar.setBounds(275, 46, 146, 50);
		contentPane.add(btnCadastrar);
		
		numeroField = new JTextField();
		numeroField.setBounds(79, 253, 184, 19);
		contentPane.add(numeroField);
		numeroField.setColumns(10);
		
		btnRemover = new JButton("REMOVER");
		btnRemover.setAction(action_1);
		btnRemover.setBounds(275, 137, 151, 50);
		contentPane.add(btnRemover);
		
		btnBuscar = new JButton("BUSCAR");
		btnBuscar.setAction(action_2);
		btnBuscar.setBounds(275, 206, 146, 50);
		contentPane.add(btnBuscar);
	}

	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "CADASTRAR");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			String nome = nomeField.getText();
			String idade = idadeField.getText();
			String peso = pesoField.getText();
			
			String cidade = cidadeField.getText();
			String estado = estadoField.getText();
			String numero = numeroField.getText();
			String logradouro = logradouroField.getText();
			
			Atleta umAtleta = new Atleta(nome);
			umAtleta.setPeso(peso);
			umAtleta.setIdade(idade);
			
			Endereco umEndereco = new Endereco(estado);
			umEndereco.setCidade(cidade);
			umEndereco.setNumero((Integer.parseInt(numero)));
			umEndereco.setLogradouro(logradouro);
			
			umAtleta.setEndereco(umEndereco);
			
			umControlador.adicionarAtleta(umAtleta);
			
			nomeField.setText("");
			idadeField.setText("");
			pesoField.setText("");
			cidadeField.setText("");
			estadoField.setText("");
			numeroField.setText("");
			logradouroField.setText("");
			
		}
	}
	private class SwingAction_1 extends AbstractAction {
		public SwingAction_1() {
			putValue(NAME, "REMOVER");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			String nome = nomeField.getText();
			
			umAtleta = umControlador.buscarAtleta(nome);
			
			umControlador.removerAtleta(umAtleta);
			
			
		}
	}
	private class SwingAction_2 extends AbstractAction {
		public SwingAction_2() {
			putValue(NAME, "BUSCAR");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			String nome = nomeField.getText();
			
			umAtleta = umControlador.buscarAtleta(nome);
			
			nomeField.setText(umAtleta.getNome());
			idadeField.setText(umAtleta.getIdade());
			pesoField.setText(umAtleta.getPeso());
			cidadeField.setText(umAtleta.getEndereco().getCidade());
			estadoField.setText(umAtleta.getEndereco().getEstado());
			numeroField.setText(umAtleta.getEndereco().getNumero() + "");
			logradouroField.setText(umAtleta.getEndereco().getLogradouro());		
			
			
		}
	}

}
