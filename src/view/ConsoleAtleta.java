package view;

import java.util.Scanner;
import model.Atleta;
import model.Endereco;
import controller.ControladorAtleta;

public class ConsoleAtleta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int opcao = -1;
		Scanner leitor = new Scanner(System.in);
		Atleta umAtleta = new Atleta();
		Endereco umEndereco = new Endereco();
		ControladorAtleta umControlador = new ControladorAtleta();
		
		while(opcao != 0){
			System.out.println("\t\t PROGRAMA ATLETA\n"
					+ "1 - Cadastrar Atleta\n"
					+ "2 - Remover Atleta\n"
					+ "3 - Buscar Atleta\n"
					+ "0 - Encerrar");
			
			opcao = leitor.nextInt();
			
			if (opcao == 1){
				System.out.println("CADASTRO SELECIONADO\n"
						+ "Digite o nome:");
				String nome = leitor.next();
				
				System.out.println("Digite a idade:");
				String idade = leitor.next();
				
				System.out.println("Digite o peso:");
				String peso = leitor.next();
				
				System.out.println("Digite o estado:");
				String estado = leitor.next();
				
				System.out.println("Digite o logradouro:");
				String logradouro = leitor.next();
				
				System.out.println("Digite o numero:");
				int numero = leitor.nextInt();
				
				System.out.println("Digite a cidade:");
				String cidade = leitor.next();
				
				//Passar os campos
				umAtleta.setNome(nome);
				umAtleta.setIdade(idade);
				umAtleta.setPeso(peso);
				
				umEndereco.setCidade(cidade);
				umEndereco.setEstado(estado);
				umEndereco.setLogradouro(logradouro);
				umEndereco.setNumero(numero);
				
				umAtleta.setEndereco(umEndereco);
				
				umControlador.adicionarAtleta(umAtleta);
			}
			
			else if (opcao == 2){
				System.out.println("REMOVER SELECIONADO!\n"
						+ "Digite o nome:");
				String removeNome = leitor.next();
				
				umAtleta = umControlador.buscarAtleta(removeNome);
				
				umControlador.removerAtleta(umAtleta);
				
			}
			
			else if (opcao == 3){
				System.out.println("BUSCAR SELECIONADO!\n"
						+ "Digite o nome:");
				String buscarNome = leitor.next();
				
				umControlador.buscarAtleta(buscarNome);
				String pausa = leitor.next();
				
			}
			else if (opcao == 4){
				System.out.println("MOSTRAR SELECIONADO!\n");
				umControlador.mostrarAtleta();
				String pausa = leitor.next();
				
			}
			else if (opcao != 1 && opcao != 2 && opcao != 3 && opcao != 4 && opcao != 0){
				System.out.println("Entrada Invalida!");
				
			}
			
		} // Fechamento LOOP
	} // Fechamento MAIN

} //Fechamento Console
