package model;

public class Atleta {
	private String nome;
	private String idade;
	private String peso;
	private Endereco endereco;
	
	
	public Atleta(String nome) {
		super();
		this.nome = nome;
	}
	
	public Atleta(){
		
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getIdade() {
		return idade;
	}


	public void setIdade(String idade) {
		this.idade = idade;
	}


	public String getPeso() {
		return peso;
	}


	public void setPeso(String peso) {
		this.peso = peso;
	}


	public Endereco getEndereco() {
		return endereco;
	}


	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
}
