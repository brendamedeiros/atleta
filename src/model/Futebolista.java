package model;

public class Futebolista extends Atleta {
	private int chuteForte;
	private int tamanhoPe;
	
	
	public Futebolista(String nome, int chuteForte) {
		super(nome);
		this.chuteForte = chuteForte;
		// TODO Auto-generated constructor stub
	}

	

	public Futebolista(String nome) {
		super(nome);
	}



	public int getChuteForte() {
		return chuteForte;
	}


	public void setChuteForte(int chuteForte) {
		this.chuteForte = chuteForte;
	}


	public int getTamanhoPe() {
		return tamanhoPe;
	}


	public void setTamanhoPe(int tamanhoPe) {
		this.tamanhoPe = tamanhoPe;
	}
	
	
	
	
}
