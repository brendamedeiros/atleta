package model;

public class JogadorBasquete extends Atleta{
	private String altura;
	private int tamanhoMao;
	
	public JogadorBasquete(String nome, String altura) {
		super(nome);
		this.altura = altura;
		// TODO Auto-generated constructor stub
	}

	public JogadorBasquete(String nome) {
		super(nome);
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public int getTamanhoMao() {
		return tamanhoMao;
	}

	public void setTamanhoMao(int tamanhoMao) {
		this.tamanhoMao = tamanhoMao;
	}
	
	
	
	
	
	
}
