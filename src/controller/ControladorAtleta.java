package controller;

import java.util.ArrayList;
import model.Atleta;

public class ControladorAtleta {
	
	ArrayList<Atleta> arrayAtleta;
	
	public ControladorAtleta(){
		arrayAtleta = new ArrayList<Atleta>();
	}
	public void adicionarAtleta(Atleta umAtleta){
		arrayAtleta.add(umAtleta);
	}
	public void removerAtleta(Atleta umAtleta){
		arrayAtleta.remove(umAtleta);
	}
	public Atleta buscarAtleta(String umNome){
		for(Atleta umAtleta: arrayAtleta){
			if(umAtleta.getNome().equalsIgnoreCase(umNome)){
				System.out.println("Nome: " + umAtleta.getNome()
						+ "\nEstado: " + umAtleta.getEndereco().getEstado());
				return umAtleta;
			}
		}
		return null;
	}
	public void mostrarAtleta(){
		for (Atleta umAtleta : arrayAtleta){
			System.out.println("Nome: " + umAtleta.getNome()
				+ "\nEstado: " + umAtleta.getEndereco().getEstado());
		}
		
	}
}
